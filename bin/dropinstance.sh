#!/bin/bash

set -eux

pg_ctl stop
rm -rf "$PGDATA"
