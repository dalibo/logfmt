#!/bin/bash
#
# Create an instance for the corresponding PG* vars and PATH.
#

set -eux

mkdir -p "$PGDATA"
initdb --username postgres
cat >"$PGDATA/postgresql.auto.conf"<<-EOF
port = ${PGPORT-5432}
shared_preload_libraries = logfmt
logging_collector = on
log_directory = 'log'
log_filename = 'postgresql-test.log'
log_error_verbosity = 'TERSE'
log_min_messages = 'INFO'
EOF
pg_ctl start
psql -c 'DROP EXTENSION IF EXISTS logfmt;'
