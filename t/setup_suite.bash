#!/bin/bash

LOGFILE="$PGDATA/log/postgresql-test.log"

setup_suite() {
	set -e
	if pg_ctl status ; then
		exit 1
	fi
	rm -rf "$PGDATA"
	bin/createinstance.sh
	retry test -f "$LOGFILE"
}

teardown_suite() {
	bin/dropinstance.sh
}

retry() {
	for s in {0..10} ; do
		if "$@" ; then
			return 0
		else
			sleep "$s"
		fi
	done

	"$@"
}
