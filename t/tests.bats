#!/bin/env bats

LOGFILE="$PGDATA/log/postgresql-test.log"

grep_logfile() {
	if grep -q "$@" "$LOGFILE" ; then
		return;
	fi

	cat "$LOGFILE" >&3
	false
}

not_grep_logfile() {
	if ! grep -q "$@" "$LOGFILE" ; then
		return;
	fi

	cat "$LOGFILE" >&3
	false
}

@test "startup logs" {
	grep_logfile '^ts=20'
	grep_logfile -F ' message="starting PostgreSQL'
}

@test "quote" {
	# ts=2023-05-30T09:48:22.178+0200 level=LOG message="listening on IPv6 address \"::1\", port 5432"
	grep_logfile -F '\"::1\", port'
}

@test "terse logs" {
	psql -c 'CREATE EXTENSION logfmt;'
	psql -c 'SELECT emit_test_logs();'
}

@test "terse output" {
	not_grep_logfile -F ' context="'
}

@test "default logs" {
	psql <<-EOF
	SET log_error_verbosity = 'DEFAULT';
	SELECT emit_test_logs();
	EOF
}

@test "context" {
	# ts=2023-05-31T08:46:21.058+0200 level=LOG message="Fusce molestie augue non finibus tristique." context="Context proin eget quam ultrices"
	grep_logfile -F ' context="Context'
}

@test "hint" {
	# ts=2023-05-30T09:51:38.919+0200 level=LOG message="Praesent nec ligula a erat tempus egestas vel a ligula." hint="Class aptent taciti sociosqu ad litora torquent."
	grep_logfile -F ' hint="Class aptent'
}

@test "multiline" {
	grep_logfile -F 'tellus.\nPellentesque'

}
