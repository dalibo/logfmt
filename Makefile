EXTENSION = logfmt
DISTVERSION := $(shell grep -Po 'default_version = .\K[\d.]+' $(EXTENSION).control)
MODULE_big = logfmt
DATA = $(wildcard $(EXTENSION)--*.sql)
OBJS = logfmt.o

PG_CONFIG ?= pg_config
PGXS = $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)

format:
	clang-format-15 -i logfmt.c

dist:
	git archive --format zip --prefix=$(EXTENSION)-$(DISTVERSION)/ -o $(EXTENSION)-$(DISTVERSION).zip HEAD
